package br.nemo.immigrant.ontology.service.eo.teams.controllers;

import br.nemo.immigrant.ontology.entity.eo.teams.models.OrganizationalRole;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationalRoleRepository;
import br.nemo.immigrant.ontology.service.eo.teams.records.OrganizationalRoleInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class OrganizationalRoleController  {

  @Autowired
  OrganizationalRoleRepository repository;

  @QueryMapping
  public List<OrganizationalRole> findAllOrganizationalRoles() {
    return repository.findAll();
  }

  @QueryMapping
  public OrganizationalRole findByIDOrganizationalRole(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public OrganizationalRole createOrganizationalRole(@Argument OrganizationalRoleInput input) {
    OrganizationalRole instance = OrganizationalRole.builder().name(input.name()).
                                                               description(input.description()).
                                                               internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public OrganizationalRole updateOrganizationalRole(@Argument Long id, @Argument OrganizationalRoleInput input) {
    OrganizationalRole instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("OrganizationalRole not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteOrganizationalRole(@Argument Long id) {
    repository.deleteById(id);
  }

}
