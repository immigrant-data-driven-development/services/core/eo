package br.nemo.immigrant.ontology.service.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "organization", path = "organization")
public interface OrganizationRepositoryWeb extends OrganizationRepository {

}
