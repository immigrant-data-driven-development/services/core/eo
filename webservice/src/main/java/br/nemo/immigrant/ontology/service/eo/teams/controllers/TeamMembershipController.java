package br.nemo.immigrant.ontology.service.eo.teams.controllers;

import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamMembershipRepository;
import br.nemo.immigrant.ontology.service.eo.teams.records.TeamMembershipInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TeamMembershipController  {

  @Autowired
  TeamMembershipRepository repository;

  @QueryMapping
  public List<TeamMembership> findAllTeamMemberships() {
    return repository.findAll();
  }

  @QueryMapping
  public TeamMembership findByIDTeamMembership(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TeamMembership createTeamMembership(@Argument TeamMembershipInput input) {
    TeamMembership instance = TeamMembership.builder().name(input.name()).
                                                       description(input.description()).
                                                       startDate(input.startDate()).
                                                       endDate(input.endDate()).
                                                       internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public TeamMembership updateTeamMembership(@Argument Long id, @Argument TeamMembershipInput input) {
    TeamMembership instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TeamMembership not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTeamMembership(@Argument Long id) {
    repository.deleteById(id);
  }

}
