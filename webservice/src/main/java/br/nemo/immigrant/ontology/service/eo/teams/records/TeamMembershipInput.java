package br.nemo.immigrant.ontology.service.eo.teams.records;
import java.time.LocalDate;
public record TeamMembershipInput( String name,String description,LocalDate startDate,LocalDate endDate,String internalId ) {
}
