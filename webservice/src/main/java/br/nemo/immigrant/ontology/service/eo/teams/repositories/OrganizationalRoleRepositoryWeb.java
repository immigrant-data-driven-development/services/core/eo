package br.nemo.immigrant.ontology.service.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.OrganizationalRole;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationalRoleRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "organizationalrole", path = "organizationalrole")
public interface OrganizationalRoleRepositoryWeb extends OrganizationalRoleRepository {

}
