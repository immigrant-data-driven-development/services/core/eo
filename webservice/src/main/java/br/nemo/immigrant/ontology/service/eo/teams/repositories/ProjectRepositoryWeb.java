package br.nemo.immigrant.ontology.service.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.ProjectRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "project", path = "project")
public interface ProjectRepositoryWeb extends ProjectRepository {

}
