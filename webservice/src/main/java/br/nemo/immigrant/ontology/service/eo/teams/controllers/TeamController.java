package br.nemo.immigrant.ontology.service.eo.teams.controllers;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;
import br.nemo.immigrant.ontology.service.eo.teams.records.TeamInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TeamController  {

  @Autowired
  TeamRepository repository;

  @QueryMapping
  public List<Team> findAllTeams() {
    return repository.findAll();
  }

  @QueryMapping
  public Team findByIDTeam(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Team createTeam(@Argument TeamInput input) {
    Team instance = Team.builder().teamPurpose(input.teamPurpose()).
                                   name(input.name()).
                                   description(input.description()).
                                   startDate(input.startDate()).
                                   endDate(input.endDate()).
                                   internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Team updateTeam(@Argument Long id, @Argument TeamInput input) {
    Team instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Team not found");
    }
    instance.setTeamPurpose(input.teamPurpose());
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTeam(@Argument Long id) {
    repository.deleteById(id);
  }

}
