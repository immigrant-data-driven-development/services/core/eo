package br.nemo.immigrant.ontology.service.eo.teams.records;
import java.time.LocalDate;
public record OrganizationalRoleInput( String name,String description,String internalId ) {
}
