package br.nemo.immigrant.ontology.service.eo.teams.controllers;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationRepository;
import br.nemo.immigrant.ontology.service.eo.teams.records.OrganizationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class OrganizationController  {

  @Autowired
  OrganizationRepository repository;

  @QueryMapping
  public List<Organization> findAllOrganizations() {
    return repository.findAll();
  }

  @QueryMapping
  public Organization findByIDOrganization(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Organization createOrganization(@Argument OrganizationInput input) {
    Organization instance = Organization.builder().name(input.name()).
                                                   description(input.description()).
                                                   internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Organization updateOrganization(@Argument Long id, @Argument OrganizationInput input) {
    Organization instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Organization not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteOrganization(@Argument Long id) {
    repository.deleteById(id);
  }

}
