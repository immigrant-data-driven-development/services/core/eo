package br.nemo.immigrant.ontology.service.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "team", path = "team")
public interface TeamRepositoryWeb extends TeamRepository {

}
