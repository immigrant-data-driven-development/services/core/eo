package br.nemo.immigrant.ontology.service.eo.teams.records;
import java.time.LocalDate;
public record OrganizationInput( String name,String description,String internalId ) {
}
