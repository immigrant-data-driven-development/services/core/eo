package br.nemo.immigrant.ontology.service.eo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.nemo.immigrant.ontology.service.eo.*"})
@EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.eo.*"})
@EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.service.eo.*"})
@OpenAPIDefinition(info = @Info(
  title = " Enterprise Ontology WebService",
  version = "1.0",
  description = "The Enterprise Ontology (EO) aims at establishing a common conceptualization on the Entreprise domain, including organizations, organizational units, people, roles, teams and projects."))

public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
