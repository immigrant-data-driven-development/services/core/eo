package br.nemo.immigrant.ontology.service.eo.teams.records;
import java.time.LocalDate;
public record PersonInput( String email,String name,String description,LocalDate startDate,LocalDate endDate,String internalId ) {
}
