package br.nemo.immigrant.ontology.service.eo.teams.controllers;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.ProjectRepository;
import br.nemo.immigrant.ontology.service.eo.teams.records.ProjectInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProjectController  {

  @Autowired
  ProjectRepository repository;

  @QueryMapping
  public List<Project> findAllProjects() {
    return repository.findAll();
  }

  @QueryMapping
  public Project findByIDProject(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Project createProject(@Argument ProjectInput input) {
    Project instance = Project.builder().name(input.name()).
                                         description(input.description()).
                                         startDate(input.startDate()).
                                         endDate(input.endDate()).
                                         internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Project updateProject(@Argument Long id, @Argument ProjectInput input) {
    Project instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Project not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteProject(@Argument Long id) {
    repository.deleteById(id);
  }

}
