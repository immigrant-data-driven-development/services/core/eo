package br.nemo.immigrant.ontology.service.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamMembershipRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "teammembership", path = "teammembership")
public interface TeamMembershipRepositoryWeb extends TeamMembershipRepository {

}
