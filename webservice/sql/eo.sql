alter table if exists organization_applications drop constraint if exists FKsobx44co0xcj2ou6ike1vl6cm;
alter table if exists organizational_role_applications drop constraint if exists FKkvw9oao6kvhfifrein3abvvv7;
alter table if exists person_applications drop constraint if exists FKb306omnaupdd9c54spjg928ab;
alter table if exists project drop constraint if exists FK56l08pkdkr9onlugrsj27t6a3;
alter table if exists project drop constraint if exists FK8bibak77woh8739ssogpu62x4;
alter table if exists project_applications drop constraint if exists FK79ms8s6l5dy4q7jxhc48315s3;
alter table if exists team drop constraint if exists FKt2rwhhxcjdmje0gqqybiyjdpn;
alter table if exists team drop constraint if exists FKp6ovpc4soflfcjbafch33w2ky;
alter table if exists team drop constraint if exists FK2br1drpqmglvo69ygcy2ex0qm;
alter table if exists team_applications drop constraint if exists FKg547qov8jsxcd73ctr90cv0vc;
alter table if exists teammembership drop constraint if exists FK32hd3ylrcn09jp4eorylbk0i0;
alter table if exists teammembership drop constraint if exists FK7twk5vq5jggy5821vfsop01ka;
alter table if exists teammembership drop constraint if exists FK143nbxei8pea2oxm87slwgsut;
alter table if exists team_membership_applications drop constraint if exists FKsx913heurcreuiqtwhp3jj33r;
drop table if exists organization cascade;
drop table if exists organization_applications cascade;
drop table if exists organizationalrole cascade;
drop table if exists organizational_role_applications cascade;
drop table if exists person cascade;
drop table if exists person_applications cascade;
drop table if exists project cascade;
drop table if exists project_applications cascade;
drop table if exists team cascade;
drop table if exists team_applications cascade;
drop table if exists teammembership cascade;
drop table if exists team_membership_applications cascade;
create table organization (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, updated_date timestamp(6), description TEXT, internal_id varchar(255) unique, name TEXT, primary key (id));
create table organization_applications (created_at timestamp(6), organization_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
create table organizationalrole (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, updated_date timestamp(6), description TEXT, internal_id varchar(255) unique, name TEXT, primary key (id));
create table organizational_role_applications (created_at timestamp(6), organizational_role_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
create table person (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, updated_date timestamp(6), description TEXT, email varchar(255), internal_id varchar(255) unique, name TEXT, primary key (id));
create table person_applications (created_at timestamp(6), person_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
create table project (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, organization_id bigint, project_id bigint, updated_date timestamp(6), description TEXT, internal_id varchar(255) unique, name TEXT, primary key (id));
create table project_applications (created_at timestamp(6), project_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
create table team (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, organization_id bigint, project_id bigint, team_id bigint, updated_date timestamp(6), description TEXT, internal_id varchar(255) unique, name TEXT, team_purpose TEXT, teamtype varchar(255) check (teamtype in ('PROJECTTEAM','ORGANIZATIONALTEAM')), primary key (id));
create table team_applications (created_at timestamp(6), team_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
create table teammembership (end_date date, start_date date, created_at timestamp(6), created_date timestamp(6), id bigserial not null, organizationalrole_id bigint, person_id bigint, team_id bigint, updated_date timestamp(6), description TEXT, internal_id varchar(255) unique, name TEXT, primary key (id));
create table team_membership_applications (created_at timestamp(6), team_membership_id bigint not null, external_id TEXT, internal_id TEXT, name TEXT);
alter table if exists organization_applications add constraint FKsobx44co0xcj2ou6ike1vl6cm foreign key (organization_id) references organization;
alter table if exists organizational_role_applications add constraint FKkvw9oao6kvhfifrein3abvvv7 foreign key (organizational_role_id) references organizationalrole;
alter table if exists person_applications add constraint FKb306omnaupdd9c54spjg928ab foreign key (person_id) references person;
alter table if exists project add constraint FK56l08pkdkr9onlugrsj27t6a3 foreign key (organization_id) references organization;
alter table if exists project add constraint FK8bibak77woh8739ssogpu62x4 foreign key (project_id) references project;
alter table if exists project_applications add constraint FK79ms8s6l5dy4q7jxhc48315s3 foreign key (project_id) references project;
alter table if exists team add constraint FKt2rwhhxcjdmje0gqqybiyjdpn foreign key (organization_id) references organization;
alter table if exists team add constraint FKp6ovpc4soflfcjbafch33w2ky foreign key (project_id) references project;
alter table if exists team add constraint FK2br1drpqmglvo69ygcy2ex0qm foreign key (team_id) references team;
alter table if exists team_applications add constraint FKg547qov8jsxcd73ctr90cv0vc foreign key (team_id) references team;
alter table if exists teammembership add constraint FK32hd3ylrcn09jp4eorylbk0i0 foreign key (organizationalrole_id) references organizationalrole;
alter table if exists teammembership add constraint FK7twk5vq5jggy5821vfsop01ka foreign key (person_id) references person;
alter table if exists teammembership add constraint FK143nbxei8pea2oxm87slwgsut foreign key (team_id) references team;
alter table if exists team_membership_applications add constraint FKsx913heurcreuiqtwhp3jj33r foreign key (team_membership_id) references teammembership;
