package br.nemo.immigrant.ontology.entity.eo.teams.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "team")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Team extends CommonConcept implements Serializable {

  @Column(columnDefinition="TEXT")
  private String teamPurpose;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "team")
  @Builder.Default
  private Set<TeamMembership> teammemberships = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "team_id")
  private Team team;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "team")
  @Builder.Default
  Set<Team> teams = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "organization_id")
  private Organization organization;

  @ManyToOne
  @JoinColumn(name = "project_id")
  private Project project;


  @Builder.Default
  @Enumerated(EnumType.STRING)
  private TeamType teamtype = TeamType.PROJECTTEAM;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Team elem = (Team) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Team {" +
         "id="+this.id+
          ", teamPurpose='"+this.teamPurpose+"'"+
          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", startDate='"+this.startDate+"'"+
          ", endDate='"+this.endDate+"'"+
          ", internalId='"+this.internalId+"'"+
          ", teamtype='"+this.teamtype+"'"+
      '}';
  }
}
