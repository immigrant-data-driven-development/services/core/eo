package br.nemo.immigrant.ontology.entity.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.repository.query.Param;
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long>, ListCrudRepository<Project, Long> {

    Optional<IDProjection> findByApplicationsExternalId(@Param("externalId") String externalId);

    Optional<IDProjection> findByInternalId(@Param("internalId") String internalId);

    Boolean existsByInternalId(@Param("internalId") String internalId);
}
