package br.nemo.immigrant.ontology.entity.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import org.springframework.data.repository.query.Param;
public interface TeamMembershipRepository extends PagingAndSortingRepository<TeamMembership, Long>, ListCrudRepository<TeamMembership, Long> {

    Optional<IDProjection> findByApplicationsExternalId(@Param("externalId")  String externalId);

   Optional<IDProjection> findByInternalId(@Param("internalId")  String internalId);

    Boolean existsByInternalId(@Param("internalId")  String internalId);

    Boolean existsByPersonAndTeam(@Param("person")  Person person,@Param("team") Team team);

}
