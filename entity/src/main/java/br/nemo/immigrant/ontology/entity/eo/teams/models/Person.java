package br.nemo.immigrant.ontology.entity.eo.teams.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Person extends CommonConcept implements Serializable {

  private String email;
  private String phoneNumber;
  private String linkedin;
  private String lattes;
  private LocalDate birthDate;
  private String address;
  private String discord;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "person")
  @Builder.Default
  private Set<TeamMembership> teammemberships = new HashSet<>();

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Person elem = (Person) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Person {" +
         "id="+this.id+
          ", email='"+this.email+"'"+
          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", phoneNumber='"+this.phoneNumber+"'"+
          ", linkedin='"+this.linkedin+"'"+
          ", lattes='"+this.lattes+"'"+
          ", birthDate='"+this.birthDate+"'"+
          ", address='"+this.address+"'"+
          ", discord='"+this.discord+"'"+
          ", internalId='"+this.internalId+"'"+

      '}';
  }
}
