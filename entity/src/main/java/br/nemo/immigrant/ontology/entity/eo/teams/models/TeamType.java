package br.nemo.immigrant.ontology.entity.eo.teams.models;

public enum TeamType {
    PROJECTTEAM,
    ORGANIZATIONALTEAM
}