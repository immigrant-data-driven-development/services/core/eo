package br.nemo.immigrant.ontology.entity.eo.teams.repositories;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.repository.query.Param;
public interface TeamRepository extends PagingAndSortingRepository<Team, Long>, ListCrudRepository<Team, Long> {

    Optional<IDProjection> findFirstByApplicationsExternalId( String externalId);
    Optional<IDProjection> findFirstByProjectName(String name);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(  String internalId);

    Boolean existsByApplicationsExternalId(String externalId);

}
