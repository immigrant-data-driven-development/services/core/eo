package br.nemo.immigrant.ontology.entity.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.repository.query.Param;
public interface OrganizationRepository extends PagingAndSortingRepository<Organization, Long>, ListCrudRepository<Organization, Long> {

    Optional<IDProjection> findByApplicationsExternalId(@Param("externalId") String externalId);

    Optional<IDProjection> findByInternalId(@Param("internalId") String internalId);

    Boolean existsByInternalId(@Param("internalId") String internalId);
}
