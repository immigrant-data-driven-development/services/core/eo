package br.nemo.immigrant.ontology.entity.eo.teams.repositories;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.Param;
public interface PersonRepository extends PagingAndSortingRepository<Person, Long>, ListCrudRepository<Person, Long> {

    Optional<IDProjection> findFirstByApplicationsExternalId(@Param("externalId")  String externalId);

    Optional<IDProjection> findFirstByInternalId(@Param("internalId")  String internalId);

    Optional<IDProjection> findFirstByEmail(@Param("email")  String email);

    Optional<IDProjection> findFirstByName(@Param("name")  String name);

    Boolean existsByInternalId(@Param("internalId")  String internalId);

    Optional<IDProjection> findByInternalId(@Param("internalId")  String internalId);


    Boolean existsByApplicationsExternalId(@Param("externalId")  String externalId);

    Boolean existsByEmail(@Param("email")  String email);

    @Modifying
    @Query("update Person p set p.email = :email where p.id = :id")
    void updateEmail(@Param(value = "id") Long id, @Param(value = "email") String email);


}
