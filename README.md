# The Enterprise Ontology (EO)
## 🚀 Goal
The Enterprise Ontology (EO) aims at establishing a common conceptualization on the Entreprise domain, including organizations, organizational units, people, roles, teams and projects.

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest
3. Spring GraphQL


## 🔧 Install

1) Create a database with name eo with **CREATE DATABASE eo**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```

### Using Jar

```xml
<dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>eo</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```

### Using Webservice

Execute docker-compose:
```bash
docker-compose up
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:](http://localhost:__«SKIP^NEW^LINE^IF^EMPTY»__) to see Swagger
* Acess [http://localhost:/grapiql](http://localhost:__«SKIP^NEW^LINE^IF^EMPTY»__/grapiql) to see Graphql.

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

* **[SEON](https://dev.nemo.inf.ufes.br/seon/)**
