# 📕Documentation: teams



## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **OrganizationalRole** : A Social Role, recognized by the Organization, assigned to Agents when they are hired, included in a team, allocated or participating in activities.
* **TeamMembership** : The allocation of a Team Member to play an Organizational Role in a Team is made through the social relation Team Membership
* **Person** : A human Physical Agent
* **Team** : A Team can be related to a Project.
* **Organization** : A Social Agent involving people and other agents and facilities with an arrangement of responsibilities, authorities and relationships
* **Project** : A Social Object as a temporary endeavor undertaken to create a unique product, service, or result.
